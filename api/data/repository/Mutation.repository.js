const MutationSchema = require("../schema/Mutation.schema");
const MutantChecker = require("../repository/MutantChecker.repository");
class MutationRepository{
  async create(mutation){ 
    mutation = new MutationSchema(mutation);
    let mutationSaved= await mutation.save();
    return mutationSaved;
  }
  async findAll(){
  let mutations=await MutationSchema.find();
  return mutations;
  }
  async findById(mutationId){
    let mutation = await MutationSchema.findOne({_id: mutationId});
    return mutation;
  }

  async findStatsBySecuence(mutationSecuence){
    let blocks = await this.hasMutation(mutationSecuence);
    const StatsOfMutation={
      blocks: blocks,
      secuence: mutationSecuence,
      isMutation: blocks.length > 1 ? true : false
    }
    return StatsOfMutation;
  }
  async hasMutation(secuence){
    var matrixTransformer = {

      getRightDiagonal : (m) => {
        var s, x, y, d,
        o = [];
        for (s = 0; s < m.length; s++) {
          d = [];
          for(y = s, x = 0; y >= 0; y--, x++)
            d.push(m[y][x]);
          o.push(d);
        }
        for (s = 1; s < m[0].length; s++) {
          d = [];
          for(y = m.length - 1, x = s; x < m[0].length; y--, x++)
            d.push(m[y][x]);
          o.push(d);
        }
        return o.map((array) => {
          return array.join('');
        });
      },
    
      getLeftDiagonal : (m) => {
        let reverse = matrixTransformer.reverseMatrix(m);
        return matrixTransformer.getRightDiagonal(reverse);
      },
    
      reverseString : (string) => {
        return string.split("").reverse().join("");
      },
    
      reverseMatrix : (m) => {
        return m.map((string) => {
          return matrixTransformer.reverseString(string);
        });
      },
    
    };
    let arrayOfMutation = [];
    let 
    secuence1,
    secuence2,
    secuence3,
    secuence4,
    secuence5,
    secuence6;
    secuence1 = secuence.slice(0,6);
    secuence2 = secuence.slice(6,12);
    secuence3 = secuence.slice(12,18);
    secuence4 = secuence.slice(18,24);
    secuence5 = secuence.slice(24,30);
    secuence6 = secuence.slice(30,36);
    arrayOfMutation.push(secuence1);
    arrayOfMutation.push(secuence2);
    arrayOfMutation.push(secuence3);
    arrayOfMutation.push(secuence4);
    arrayOfMutation.push(secuence5);
    arrayOfMutation.push(secuence6);
    const mutantCheckRepo = new MutantChecker(matrixTransformer);
    let blocks = mutantCheckRepo.findMutantBlocks(arrayOfMutation);
    return blocks;
  }
}
module.exports = MutationRepository;