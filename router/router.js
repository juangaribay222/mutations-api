const express= require("express");
const router = express.Router();
const MutationController = require("../api/controller/Mutation.controller");
const StatsController = require("../api/controller/Stats.controller");

const statsController=new StatsController();
const mutationController=new MutationController();

router.post("/mutations",mutationController.create);
router.get("/mutations",mutationController.findAll);
router.get("/mutations/:mutationId",mutationController.findById);
router.get("/hasMutation/:secuence",mutationController.hasMutation);
router.get("/stats/:secuence",statsController.findStatsBySecuence);
router.get("/stats",statsController.findAllStats);
router.get("/",(req,res)=>{
    res.send("Hola mundo");
});
module.exports=router;