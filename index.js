
const express=require("express");
const bodyParser=require("body-parser");
const router= require("./router/router");
const requestconfigs=require("./requestConfig");
require("./dbconnection");
require('dotenv').config({
    path:'vars.env'
})
const app=express();
const port= process.env.PORT || 8050;
const host = process.env.HOST || '0.0.0.0'
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(requestconfigs.setHeaders);
app.use("/",router);

app.listen(port,()=>{
    
    console.log(`servidor levantado en ${port}`);
});
app.on('clientError', (err, socket) => {
    console.error(err);
  socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
}) 