const mongoose=require("mongoose");
require('dotenv').config({
    path:'vars.env'
})
const connectionString = process.env.DB_URL
mongoose.connect(connectionString);
mongoose.connection.on("connected",()=>{
    console.log("Mongo connected");
});
mongoose.connection.on("disconnected",()=>{
    console.log("Mongo disconnected");
});
mongoose.connection.on("error",(err)=>{
    console.log("Mongo error" + err);
});
