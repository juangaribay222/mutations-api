const MutationRepository = require("../data/repository/Mutation.repository");
const mutationRepo = new MutationRepository();
class StatsController{
    async findStatsBySecuence(request, response){
        let mutationSecuence = request.params.secuence;
        let statsResponse;
        let stats = await mutationRepo.findStatsBySecuence(mutationSecuence)
        .then(res=>{
            statsResponse = res;
            response.status(200).send(statsResponse);
        })
        .catch(err => {
            response.status(403).send("Error")
        })
    }
    async findAllStats(request, response){
        try {
            let stats = await mutationRepo.findAll()
            .then()
            .catch();
            response.status(200).json(stats);
        } catch (error) {
            console.error(error);
        }
    }
}
module.exports = StatsController;