const MutationRepository = require("../data/repository/Mutation.repository");
const mutationRepo = new MutationRepository();

class MutationController{ 
    async findById(request, response){
        let mutationId = request.params.mutationId;
        let mutation = await mutationRepo.findById(mutationId);
        if (mutation)
            response.status(200).send(mutation);
        else
            response.status(403).send("Error")
        
    }
    async findAll(request, response){
        try {
         
            let mutations = await mutationRepo.findAll();
            response.status(200).json(mutations);
        } catch (error) {
            console.error("Error" + error);
        }
    }
    async create(reqest,response){
        try{
        let mutation = reqest.body;
        if(mutation._id || mutation._id===""){
            delete mutation._id;
        }
        let mutationSaved=await mutationRepo.create(mutation);
        response.status(200).send(mutationSaved);
        }
        catch (error){
            response.status(403).json("Error"+error);
        }
    }
    async hasMutation(request, response){
        let secuence = request.params.secuence;
        let mutationResponse;
        let isMutant = await mutationRepo
        .findStatsBySecuence(secuence)
        .then(res => {
            mutationResponse = res;
            response.status(200).json(mutationResponse);
        })
        .catch(err => {
            response.status(403).send("error" + err);
        })
        if(isMutant)
            return isMutant
        return false;
    }
}
module.exports = MutationController;